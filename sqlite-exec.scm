(use-modules (sqlite3))
(define db (sqlite-open "mydb" (logior SQLITE_OPEN_CREATE SQLITE_OPEN_READWRITE)))
(define sql "SELECT SQLITE_VERSION();
             CREATE TABLE Cars(Id INT, Name TEXT, Price INT);
             INSERT INTO Cars VALUES(1, 'Audi', 52642);
             INSERT INTO Cars VALUES(2, 'Mercedes', 57127);")
(sqlite-exec db sql)
